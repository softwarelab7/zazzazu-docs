#!/bin/bash

function create-venv() {
	mkvirtualenv zazzazu-docs
}

function venv-workon() {
	workon zazzazu-docs
}