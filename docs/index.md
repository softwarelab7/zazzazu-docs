
# Welcome to Zazzazu App Docs
This is the documentation on how to manage the administration site of Zazzazu.

## Administration Guide

* [Getting Started](login)
	
	* [Logging In](login)<br></br>


* [Dashboard](dashboard) 
	
	* [Store Statistics](dashboard#store-statistics)

	* [Orders - Last 24 Hours](dashboard#orders-last-24-hours)

	* [Orders - All Time](dashboard#orders-all-time) 

	* [Customers](dashboard#customers) 

	* [Catalogue and Stock](dashboard#catalogue-and-stocks)

	* [Offers, Vouchers and Promotions](dashboard#offers,-vouchers-and-promotions) <br></br>


* [Product Management/Catalogue](product)

	* [Looks Products/Services](product#looks-product-services)

	* [How to Add New Service or Product](product#how-to-add-new-service-or-product)

	* [How to Create Skills Offered](product#how-to-create-skills-offered) 

	* [How to Edit Skills Offered](product#how-to-edit-skills-offered) 

	* [How to Edit Product Information](product#how-to-edit-product-information)

	* [How to Delete Product/Service](product#how-to-delete-product-service) 

    <br></br>


* [Order Management](orders)

	* [How to View Orders](orders#how-to-view-orders)

	* [How to View Order Details of Specific Order](orders#how-to-view-order-details-of-specific-order)

	* [How to View Appointment Details of an Order](orders#how-to-view-appointment-details-of-an-order)

	* [How to View All Appointments](orders#how-to-view-all-appointments)

	* [Appointment Status of an Order](orders#appointment-status-of-an-order)

	* [How to Cancel an Appointment via Web](orders#how-to-cancel-an-appointment-via-web)

	* [How to Change Attending Stylist of a Certain Appointment](orders#how-to-change-attending-stylist-of-a-certain-appointment) <br></br>



* [Users Management](users)

	* [Users Status](users#users-status)

	* [Types of Users](users#types-of-users)	

	* [User Management](users#user-management)

	* [How to Change User Status or Type](users#how-to-change-user-status-or-type) 

	* [Waitlist](users#waitlist) 

	* [How to do Password Reset](users#how-to-do-password-reset) <br></br>



* [All About Stylist](stylist)

	* [All About Stylist Management](stylist#all-about-stylist-management)

	* [Partners or Stylists](stylist#partners-or-stylists)

	* [Set Stylist Account As Reviewed](stylist#set-stylist-account-as-reviewed)

	* [How to Check Stylist Coverage](stylist#how-to-check-stylist-coverage)

	* [How to View Stylist Schedules](stylist#how-to-view-stylist-schedules)

	* [How to View Stylist Schedules](stylist#how-to-view-stylist-work-schedules)

	* [Stylist Bank Account](stylist#stylist-bank-account)

	* [How to Update Stylist Bank Account via Web](stylist#how-to-update-stylist-bank-account-via-web) <br></br>


* [Promotions](promo)

	* [Promotions](promo#promotions)

	* [How to Create Range for All Products](promo#how-to-create-range-for-all-products)

	* [How to Create Range for Specific Products or Services](promo#how-to-create-range-for-specific-products-or-services)

	* [How to Edit Ranges](promo#how-to-edit-ranges)

	* [How to Delete Ranges](promo#how-to-delete-ranges)

	* [Creating New Promo Code](promo#creating-new-promo-code)

	* [How to View Details of your Promo Code](promo#how-to-view-details-of-your-promo-code)

	* [How to Edit Promo Code](promo#how-to-edit-promo-code)

	* [How to Delete Promo Code](promo#how-to-delete-promo-code) 	<br></br>

* [Coverage](coverage)

	* [Coverage](coverage)

	* [Adding a Zip Code to Coverage](coverage#adding-a-zip-code-to-coverage)

	* [How to Delete Zip Code from Coverage](coverage#how-to-delete-zip-code-from-coverage) <br></br>


* [Time Padding](padding) 

	* [Time Padding](padding) 

	* [How to Set Appointment Padding](padding#how-to-set-appointment-padding) 

	* [How to Set Padding From Current Date and Time](padding#how-to-set-padding-from-current-date-and-time) <br></br>

* [Reports and Statistics](reports) 

	* [Reports](reports#reports) 

	* [Ordered Placed](reports#ordered-placed) 

	* [Product Analytics](reports#product-analytics) 

	* [User Analytics](reports#user-analytics) 

	* [Open Baskets](reports#open-baskets) 

	* [Submitted Baskets](reports#submitted-baskets) 

	* [Voucher Performance](reports#voucher-performance) 

	* [Offer Performance](reports#offer-performance) 

	* [Statistics](reports#statistics) <br></br>


* [Stripe Balance](stripe) 

	* [Stripe Balance](stripe#stripe-balance) 

	* [How to View Stripe Transactions](stripe#how-to-view-stripe-transactions) 



	* [How to Add Stripe Balance](stripe#how-to-add-stripe-balance) <br></br>

* [All About Stylist](stylistv2)

	* [All About Stylist Management New Features](stylistv2#all-about-stylist-management-new-features)

	* [How to Change Attending Stylist of a Certain Appointment by Skill](stylistv2#how-to-change-attending-stylist-of-a-certain-appointment-by-skill)

	* [How to see Stylist Change History](stylistv2#how-to-see-stylist-change-history)

	* [How to Update Stylist Bank Information](stylistv2#how-to-update-stylist-bank-information)

	* [How to Create Stylist Color](stylistv2#how-to-create-stylist-color)

	* [How to Edit Stylist Color](stylistv2#how-to-edit-stylist-color)

	* [Stylist Schedule Management](stylistv2#stylist-schedule-management)

	* [How to Create Stylist Work Schedules](stylistv2#how-to-create-stylist-work-schedules)

	* [How to Edit Stylist Work Schedules](stylistv2#how-to-edit-stylist-work-schedules)

	* [How to Delete Stylist Work Schedules](stylistv2#how-to-delete-stylist-work-schedules)

	* [How to Create Unavailable Schedule for Stylist](stylistv2#how-to-create-unavailable-schedule-for-stylist)

	* [How to Edit Unavailable Schedule for Stylist](stylistv2#how-to-edit-unavailable-schedule-for-stylist)

	* [How to Delete Unavailable Schedule for Stylist](stylistv2#how-to-delete-unavailable-schedule-for-stylist)

	<br></br>

## IOS Zazzazu Guide

* [User Guide](userios) 

	* [Pre-requisite](userios#prerequisite) 

	* [How to Register through Facebook Account](userios#how-to-register-through-facebook-account) 

	* [How to Register Using Email Address](userios#how-to-register-using-email-address) 

	* [Understanding Zazzazu Menu](userios#understanding-Zazzazu-menu) 

	* [How to Browse Styles](userios#how-to-browse-styles) 

	* [How to Book an Appointment](userios#how-to-book-an-appointment) 

	* [How to View Your Calendar](userios#how-to-view-your-calendar) 

	* [How to Cancel or Modify an Appointment](userios#how-to-cancel-or-modify-an-appointment) 

	* [How to Take the Survey](userios#how-to-take-the-survey) <br></br>


* [Stylist Guide](stylistios) 

	* [Pre-requisite Set Up for a Stylist Account](stylistios#pre-requisite-set-up-for-a-stylist-account) 

	* [Register As Stylist](stylistios#register-as-stylist) 

	* [Setting Up Your Stylist Account via App](stylistios#setting-up-your-stylist-account-via-app) 

	* [How to Update Stylist Bank Account via App](stylistios#how-to-update-stylist-bank-account-via-app)

	* [Setting UP Your Stylist Schedule via App](stylistios#setting-up-your-stylist-schedule-via-app)  

	* [How to Edit Your Stylist Schedule via App](stylistios#how-to-edit-your-stylist-schedule-via-app) 

	* [How to Schedule Unavailable Time](stylistios#how-to-schedule-unavailability-time) <br></br>