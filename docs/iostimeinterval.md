### How To Adjust Available Time Interval on IOS app using Web Dashboard.

1. First, log in using your Admin credentials.

2. Click _**Fulfillment**_.

	![coverage](/img/coverage.png) <br></br>

3. Click _**Available Schedules Interval**_.

	![timeinterval](/img/availableinterval.png) <br></br>

	You will be directed to this page.

	![timeinterval](/img/availableinterval1.png) <br></br>

4. Change the number under the box Minutes, that will determine the **Available booking appointment intervals**. Then click Save.

	![timeinterval](/img/availableinterval2.png) <br></br>

	You will be directed to this page.

	![timeinterval](/img/availableinterval3.png) <br></br>

	Here is what you are going to see on available schedules for booking on the app.

	![timeinterval](/img/availableinterval4.png) <br></br>

