### All about Stylist Management

* Partners - These are the stylists.

* Stylist Coverages - These are the coverage set by the stylist, how far they can go to cater the service needed in radius. <br></br>

* Stylist Schedules - These are the booked schedules of the stylist for the day, week or month. <br></br>

* Stylist Work Schedules - These are the schedules set by the stylist on their Zazzazu App. <br></br>

* Stylist Bank Accounts - This is set by the stylist on their Zazzazu app. This is where they are going to be paid. <br></br>

### Partners or Stylists

1. Click _**Fulfillment**_.

	![coverage](/img/coverage.png) <br></br>

2. Click _**Partner Stylists**_.

	![Stylist](/img/stylist.png) <br></br>

	You will be directed to this page, wherein you will see all the active stylist on Zazzazu App.

	![Stylist](/img/stylist1.png) <br></br>

**Note**: Stylist accounts are set via admin web.


### How to Set User as a Stylist

1. Click _**Users**_.

	![Users](/img/users.png) <br></br>

2. Click _**Users**_.

	![Users](/img/users1.png) <br></br>

	You will be directed to this page, where you will see all users on the system.

	![Users](/img/users2.png) <br></br>

3. Click _**Change Status the email address**_ of the user you want to change _**Status**_ or _**User Type**_ or both.

	![Stylist](/img/stylist2.png) <br></br>

4. Click _**Go**_ to set the user account as a stylist account.

	![Stylist](/img/stylist3.png) <br></br>

	You will be directed to this confirmation page.

	![Stylist](/img/stylist4.png) <br></br>	


To check if everything is up and working for the stylist, go to [Partners](#partners-or-stylist). You should see the name of the person you have Set as stylist. Meaning you have confirmed that, that person is one of your stylist.

![Stylist](/img/stylist5.png) <br></br>

### How to Check Stylist Coverage

1. Click _**Fulfillment**_.

	![coverage](/img/coverage.png) <br></br>

2. Click _**Stylist Coverages Map**_. 

	![Stylist](/img/stylist6.png) <br></br>

	You will be directed to this page, wherein you can see a map and the radius of coverage of each of your stylist.

	![Stylist](/img/stylist7.png) <br></br>	

3. The red circles are your stylist coverage, now if the addresses of your stylist are near each other, they might overlapped. If that is the case, you may zoom in to see all the stylist coverage by scrolling your mouse forward or clicking _**+**_ sign on the lower right corner of your screen.

	![Stylist](/img/stylist8.png) <br></br>	

	These 3 addresses are near each other reason why they overlapped. We zoomed in so we can see each icon individually.

	![Stylist](/img/stylist9.png) <br></br>	

	Click the _**Icon**_ for you to be able to see the address and username of the stylist.

	![Stylist](/img/stylist10.png) <br></br>	



### How to View Stylist Schedules

* When a stylist is booked for a job, it will generate a schedule that we can see on the  web using admin/staff access. Here is how to view the stylists booked schedules.

1.  Click _**Schedules**_.

	![Stylist](/img/stylistwork.png) <br></br>	

2. Click _**Stylist Schedules**_.

	![Stylist](/img/stylistwork1.png) <br></br>	

	You will be directed to this page.

	![Stylist](/img/stylistwork2.png) <br></br>	

3. From here you can filter the name of the Stylist you want to check schedules for the month, week or day. You can also filter this via city and or service and the schedule type. Just click _**Filter**_ to be able to see the page you want to see.

	![Stylist](/img/stylistwork3.png) <br></br>	



### How to see Stylist Work Schedules

* Stylist Work Schedules are made by stylists on their IOS app. As admins/Staff we can also see their schedule via this web.

1.  Click _**Schedules**_.

	![Stylist](/img/stylistwork.png) <br></br>	

2. Click _**Stylist Work Schedules**_.

	![Stylist](/img/stylistwork4.png) <br></br>	

	You will be directed to this page wherein you will see the schedules of all of your stylists.

	![Stylist](/img/stylistwork5.png) <br></br>	


### Stylist Bank Account

* Stylist Bank accounts are normally created using Zazzazu App. On the web we can see if the stylist updated their account using Zazzazu app or not yet. Their name will appear on the screen.

* See [Setting up your Stylist Account via App](stylistios#setting-up-your-stylist-account-via-app) for more details how to create one.

	
### How to Update Stylist Bank Information

Updating Stylist Bank Information is used for bank account authentication for our stylist bank accounts, so that we can transfer their payments without any concerns. 

1. Click _**Stylist Bank Account**_.

	![Stylist](/img/stylistbank.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/stylistbank1.png) <br></br>

2. Click _**Actions**_.

	![Stylist](/img/stylistbank2.png) <br></br>

3. Click _**Update Bank Account**_.

	![Stylist](/img/stylistbank3.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/stylistbank4.png) <br></br>	

4. Fill out the form with the new bank account the stylist have, then click _**Save**_.

	![Stylist](/img/stylistbank5.png) <br></br>

	Confirmation will be seen.

	![Stylist](/img/stylistbank6.png) <br></br>



### How to View Stylist Bank Information

1. Click _**Stylist Bank Account**_.

	![Stylist](/img/stylistbank.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/stylistbank1.png) <br></br>

2. Click _**Actions**_.

	![Stylist](/img/stylistbank2.png) <br></br>

3. Click _**View Stripe Account Details**_.

	![Stylist](/img/stylistbank7.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/stylistbank8.png) <br></br>


### How to Change Attending Stylist of a Certain Appointment by Skill

* Stylist of each order are randomly selected by the system. Sometimes we have customers who are requesting for a certain stylist or when our stylist have some emergency we will need to change the stylist attending to a certain appointment. Here is how to change attending stylist.

	1. First, log in using your Admin credentials.

	2. Click _**Fulfillment**_.

		![coverage](/img/coverage.png) <br></br>

	3. Click _**Orders**_.

		![Orders](/img/orders.png) <br></br>	

		Then you will be directed to this page.

		![Orders](/img/orders1.png) <br></br>

	4. Click _**View**_ beside the order number you want to check the appointment.

		![Orders](/img/changestylist.png) <br></br>

		Then you will be directed to this page.

		![Orders](/img/changestylist1.png) <br></br>


	5. Click _**View Appointment**_.

		![Orders](/img/changestylist2.png) <br></br>

		Then you will be directed to this page.

		![Orders](/img/changestylist3.png) <br></br>


	6. Click _**Change Stylist**_.

		![Change Stylist](/img/changestylist4.png) <br></br>

		You will be directed to this page. As you can see below, the current stylist name and details are listed on the page so that you can see who is the current stylist, appointment details, and the skill of the stylist. 

		![Change Stylist](/img/changestylist5.png) <br></br>

	6. Click _**Stylist**_. You will see a drop down of all the stylist available for that time and date. The name of the current stylist is listed below as well for your reference.

		![Change Stylist](/img/changestylist6.png) <br></br>

		Note: The list of names you will see on this dropdown, are the stylists who don't have any appointment at that date and time of the order. If there are none, it just means that all stylist have an appointment at that time. <br></br>

	7. Click the name of stylist, then click _**Save**_ to change the stylist.

		![Change Stylist](/img/changestylist7.png) <br></br>

		You will see a confirmation that the stylist was changed. From Ashley Maine to Test Testing.

		![Change Stylist](/img/changestylist8.png) <br></br>


### How to see Stylist Change History

1. First, log in using your Admin credentials.

2. Click _**Fulfillment**_.

	![coverage](/img/coverage.png) <br></br>

3. Click _**Orders**_.

	![Orders](/img/orders.png) <br></br>	

	Then you will be directed to this page.

	![Orders](/img/orders1.png) <br></br>

4. There are 2 ways on how you can view Order Details of a specific order you want to check.

	* Click the _**Order Number**_.

	![Orders](/img/orders2.png) <br></br>	

	* Click _**View**_ beside the order number you want to check, then click _**View Orders**_.

	![Orders](/img/orders3.png) <br></br>	

	* Any of the two ways will direct you to this page. Here you can see the complete order details of the specific order. 

		- **Customer Information** - This is where you will see customer's name and Email address of the person who ordered the service/product.

		![Orders](/img/orders4.png) <br></br>	

		- **Order Information** - This is where you will see the total price of the order, date it was purchased, time it was purchased and its status.

		![Orders](/img/orders5.png) <br></br>	

		- **Order Details** - This is where you can see the price of the product/service, if there are any promotions used by the customer and total amount paid by the customer.

		![Orders](/img/orders6.png) <br></br>

		**Payment tab** will show payment transactions. The Amount _**Transferred**_ is the money being transferred to the Stylist. The Amount _**Settled**_ is the money paid by the customer.

		![Orders](/img/orders7.png) <br></br>	

		**Discount Tab** will show you what promotions is or was used by the customer if there are any.

		![Orders](/img/orders8.png) <br></br>	

		**Notes Tab** this is where you will see any notes regarding the order and the change of stylist made on the order every change will be listed here, you can also add a note about the order or the change of stylist on this page. 

		![Change Stylist](/img/changestylist9.png) <br></br>

		NOTE: Please be reminded that after 5 minutes, any notes that is put on this tab will be a permanent note on the order.


### How to Create Stylist Color

1. Click _**Schedules**_.

	![Stylist](/img/stylistcolors.png) <br></br>

2. Click _**Stylist Colors**_.

	![Stylist](/img/stylistcolors1.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/stylistcolors2.png) <br></br>

3. Click _**+Create new color**_.

	![Stylist](/img/stylistcolors3.png) <br></br>

4. Click the hyperlink that says **"Click here to see Color codes"**.

	![Stylist](/img/stylistcolors4.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/stylistcolors5.png) <br></br>


5. Browse the page until you can see the picture below. 

	![Stylist](/img/stylistcolors6.png) <br></br>

6. Pick the color you want, by clicking the **Color**.

	![Stylist](/img/stylistcolors7.png) <br></br>

	You will see the code above right after you click the color you want. 

	![Stylist](/img/stylistcolors7.png) <br></br>

7. Copy the code you will see below the small box of the color you have choosen. 

	![Stylist](/img/stylistcolors8.png) <br></br>

8. Go back to the browser where Zazzazu Dashboard is at.

	![Stylist](/img/stylistcolors3.png) <br></br>

9. Paste the color code you have copied from the other webpage, to **Code**.

	![Stylist](/img/stylistcolors9.png) <br></br>

10. Choose the stylist you want to have the color from the stylist dropdown. Click **Stylist**.

	![Stylist](/img/stylistcolors10.png) <br></br>

11. Click _**Save**_ to save the changes.

	![Stylist](/img/stylistcolors11.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/stylistcolors12.png) <br></br>	


	Note: You will see the stylist colors on **Dashboard/Stylists Schedules**, each colors represents a particular stylist. 

	![Stylist](/img/stylistcolors13.png) <br></br>	


### How to Edit Stylist Color

1. Click _**Schedules**_.

	![Stylist](/img/stylistcolors.png) <br></br>

2. Click _**Stylist Colors**_.

	![Stylist](/img/stylistcolors1.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/stylistcolors2.png) <br></br>

3. Click _**Edit**_ beside the stylist you want to edit colors with.

	![Stylist](/img/stylistcolors14.png) <br></br>	

	You will be directed to this page.

	![Stylist](/img/stylistcolors15.png) <br></br>

4. Here you can edit the Color Code or the stylist for the color code. 

 	![Stylist](/img/stylistcolors16.png) <br></br>


5. Click _**Save**_, to save the changes.

 	![Stylist](/img/stylistcolors17.png) <br></br>

 	You will be directed to this page.

	![Stylist](/img/stylistcolors18.png) <br></br>

	Note: You will see the stylist colors on **Dashboard/Stylists Schedules**, each colors represents a particular stylist. 

	![Stylist](/img/stylistcolors13.png) <br></br>	


### Stylist Schedule Management

All Stylist Schedules can be edited through Admin Access and will be directly implemented to Zazzazu Mobile App.

1. Stylist Work Schedule.
	
	* Admin can create stylist work schedule via web dashboard once they are set as reviewed.

	* Admin can edit stylist work schedule via web dashboard once they are set as reviewed.

	* Admin can delete stylist work schedule via web dashboard once they are set as reviewed. <br></br>	

2. Stylist Unavailable Schedule.

	* Admin can create stylist unavailable schedule via web dashboard once they are set as reviewed.

	* Admin can edit stylist unavailable schedule via web dashboard once they are set as reviewed.

	* Admin can delete stylist unavailable schedule via web dashboard once they are set as reviewed. <br></br>	


### How to Create Stylist Work Schedules 

1. Click _**Schedules**_.

	![Stylist](/img/stylistcolors.png) <br></br>

2. Click _**Stylist Work Schedules**_.

	![Stylist](/img/stylistworkschedule.png) <br></br>

 	You will be directed to this page.

	![Stylist](/img/stylistworkschedule1.png) <br></br>

3. Click _**Create Stylist Work Schedules**_.

	![Stylist](/img/stylistworkschedule2.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/stylistworkschedule3.png) <br></br>

4. Fill out the Start, End, Weekdays and Stylist you want the schedule to belong to. 


	![Stylist](/img/stylistsworkschedule5.png) <br></br>

	Weekdays is a dropdown where you can select multiple days. To select multiple days, press **CMD** or **CTRL** then click the days you want to add.

	![Stylist](/img/stylistworkschedule4.png) <br></br>

	**Note**: stylists can be seen on Stylist dropdown. <br></br>


5. Click _**Save**_, to save the changes. 

	![Stylist](/img/stylistworkschedule6.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/stylistworkschedule7.png) <br></br>




### How to Edit Stylist Work Schedules


1. Click _**Schedules**_.

	![Stylist](/img/stylistcolors.png) <br></br>

2. Click _**Stylist Work Schedules**_.

	![Stylist](/img/stylistworkschedule.png) <br></br>

 	You will be directed to this page.

	![Stylist](/img/stylistworkschedule1.png) <br></br>

3. Click _**Edit**_ beside the name of the stylist you want to edit schedule.

	![Stylist](/img/stylistworkschedule8.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/stylistworkschedule9.png) <br></br>

4. Click _**Action**_ beside the day of the work schedule you want to edit.

	![Stylist](/img/stylistworkschedule10.png) <br></br>

5. Click _**Edit**_.

	![Stylist](/img/stylistworkschedule11.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/stylistworkschedule12.png) <br></br>

6. Edit the Start and/or End time and or the day you want to edit, this will supersede the initial schedule set for that day or days.

	![Stylist](/img/stylistworkschedule13.png) <br></br>

7. Click _**Save**_, this will save the changes you made on the schedule.

	![Stylist](/img/stylistworkschedule14.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/stylistworkschedule15.png) <br></br>




### How to Delete Stylist Work Schedules

1. Click _**Schedules**_.

	![Stylist](/img/stylistcolors.png) <br></br>

2. Click _**Stylist Work Schedules**_.

	![Stylist](/img/stylistworkschedule.png) <br></br>

 	You will be directed to this page.

	![Stylist](/img/stylistworkschedule1.png) <br></br>

3. Click _**Edit**_ beside the name of the stylist you want to edit schedule.

	![Stylist](/img/stylistworkschedule8.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/stylistworkschedule9.png) <br></br>

4. Click _**Action**_ beside the day of the work schedule you want to delete.

	![Stylist](/img/stylistworkschedule10.png) <br></br>

5. Click _**Delete**_.

	![Stylist](/img/stylistworkschedule16.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/stylistworkschedule17.png) <br></br>

6. Click _**Delete**_, this will confirm that you are deleting the schedule.

	![Stylist](/img/stylistworkschedule18.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/stylistworkschedule19.png) <br></br>


### How to Create Unavailable Schedule for Stylist

1. Click _**Schedules**_.

	![Stylist](/img/stylistcolors.png) <br></br>

2. Click _**Stylist Schedules**_.

	![Stylist](/img/unvailsched.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/unavailsched1.png) <br></br>

3. Hover your mouse to the date you want to set unavailable schedule for stylist. You will see a _**+**_ sign.

	![Stylist](/img/unavailsched2.png) <br></br>

4. Click _**+**_ sign.

	![Stylist](/img/unavailsched3.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/unavailsched4.png) <br></br>

5. Fill out the Title of the unavailable schedule, you can use emoticons/emoji if you want too, Start date, End date and choose from the drop down the stylist you want the unavailable schedule for.

	![Stylist](/img/unavailsched5.png) <br></br>

6. Click _**Save**_, to save the unavailable schedule, this update will directly reflect on the stylist Zazzazu mobile app as well.

	![Stylist](/img/unavailsched6.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/unavailsched7.png) <br></br>


### How to Edit Unavailable Schedule for Stylist

1. Click _**Schedules**_.

	![Stylist](/img/stylistcolors.png) <br></br>

2. Click _**Stylist Schedules**_.

	![Stylist](/img/unvailsched.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/unavailsched1.png) <br></br>

3. Click Unavailable Schedule you want to edit from the calendar.

	![Stylist](/img/unavailsched8.png) <br></br>

4. Click _**Blue description of the unavailable schedule**_.

	![Stylist](/img/unavailsched9.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/unavailsched10.png) <br></br>

5. Click _**Edit**_.

	![Stylist](/img/unavailsched11.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/unavailsched12.png) <br></br>

6. Edit the detail you want to edit, like Title, Start date, Start time, End date and End time.

	![Stylist](/img/unavailsched12.png) <br></br>

7. Click _**Save**_, to save changes.

	![Stylist](/img/unavailsched13.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/unavailsched14.png) <br></br>



### How to Delete Unavailable Schedule for Stylist

1. Click _**Schedules**_.

	![Stylist](/img/stylistcolors.png) <br></br>

2. Click _**Stylist Schedules**_.

	![Stylist](/img/unvailsched.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/unavailsched1.png) <br></br>

3. Click Unavailable Schedule you want to delete from the calendar.

	![Stylist](/img/unavailsched8.png) <br></br>

4. Click _**Blue description of the unavailable schedule**_.

	![Stylist](/img/unavailsched9.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/unavailsched10.png) <br></br>

5. Click _**Delete**_.

	![Stylist](/img/unavailsched15.png) <br></br>

	You will see a pop up message.

	![Stylist](/img/unavailsched16.png) <br></br>

6. Click _**OK**_, this will delete the unavailable schedule permanently.

	![Stylist](/img/unavailsched17.png) <br></br>

	You will see a pop up message.

	![Stylist](/img/unavailsched18.png) <br></br>



### How to Add/Create Stylist Address via Admin Web Dashboard

1. Click _**Users**_.

	![Stylist](/img/stylistaddress.png) <br></br>

2. Click _**Stylist Addresses**_.

	![Stylist](/img/stylistaddress1.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/stylistaddress2.png) <br></br>

3. Click _**+Create new stylist address**_.

	![Stylist](/img/stylistaddress3.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/stylistaddress4.png) <br></br>

4. Fill out the form by putting in the stylist address, Address line 2 is for **Apartment/Suite Number or something like that**, Address, and from the dropdown the stylist.

	![Stylist](/img/stylistaddress5.png) <br></br>

5. Click _**Save**_.

	![Stylist](/img/stylistaddress6.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/stylistaddress7.png) <br></br>

NOTE: Automatically the newly saved Stylist address will be the stylist default address, that address will be the reference for stylist radius coverage and this change will automatically update the stylist address on Zazzazu Mobile App.


### How to Edit Stylist Address via Admin Web Dashboard

1. Click _**Users**_.

	![Stylist](/img/stylistaddress.png) <br></br>

2. Click _**Stylist Addresses**_.

	![Stylist](/img/stylistaddress1.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/stylistaddress2.png) <br></br>

3. Click _**Actions**_ beside the name and address of the stylist you want to edit the address.

	![Stylist](/img/stylistaddress8.png) <br></br>

4. Click _**Edit**_.

	![Stylist](/img/stylistaddress9.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/stylistaddress10.png) <br></br>

5. Edit the address.

	![Stylist](/img/stylistaddress11.png) <br></br>

6. Click _**Save**_.

	![Stylist](/img/stylistaddress12.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/stylistaddress13.png) <br></br>


**NOTE**: Editing stylist address will not change the one that is on the stylist default address or on Zazzazu Stylist Settings.


### How to Delete Stylist Address via Admin Web Dashboard

1. Click _**Users**_.

	![Stylist](/img/stylistaddress.png) <br></br>

2. Click _**Stylist Addresses**_.

	![Stylist](/img/stylistaddress1.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/stylistaddress2.png) <br></br>

3. Click _**Actions**_ beside the name and address of the stylist you want to delete the address.

	![Stylist](/img/stylistaddress8.png) <br></br>

4. Click _**Delete**_.

	![Stylist](/img/stylistaddress14.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/stylistaddress16.png) <br></br>

	Or this page, whenever you will delete a stylist address that have ordered anything from the system as a customer or appointment when that was the stylist address the system will not let you delete the address.

	![Stylist](/img/stylistaddress15.png) <br></br>

5. Click _**Delete**_, to permanently delete the address.

	![Stylist](/img/stylistaddress17.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/stylistaddress18.png) <br></br>

### How to Set/Change/Edit Stylist Default Address

All stylist will be on the list of Stylist Default Addresses regardless if they have a schedule or address put in on Zazzazu App. This default address will serve as the reference point of their stylist raduis or the range of area the stylist can cater.

1. Click _**Users**_.

	![Stylist](/img/stylistaddress.png) <br></br>

2. Click _**Stylist Default Addresses and Coverages**_.

	![Stylist](/img/stylistdefaultaddd.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/stylistdefaultadd1.png) <br></br>

3. Click _**Actions**_.

	![Stylist](/img/stylistdefaultadd2.png) <br></br>

3. Click _**Edit Default Address**_.

	![Stylist](/img/stylistdefaultadd3.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/stylistdefaultadd4.png) <br></br>

4. Choose from the address dropdown the address you want the stylist to have as its default address.

	![Stylist](/img/stylistdefaultadd5.png) <br></br>

5. Click _**Save**_.

	![Stylist](/img/stylistdefaultadd6.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/stylistdefaultadd7.png) <br></br>

### How to View Stylist Radius via Admin Web Dashboard

There are two ways on how to view Stylist Radius on Admin Web Dashboard.

* Stylist Coverage

	1. Click _**Fulfillment**_.

		![coverage](/img/coverage.png) <br></br>

	2. Click _**Stylist Coverage**_.

		![Stylist](/img/stylist6.png) <br></br>

		You will be directed to this page.

		![Stylist](/img/stylist7.png) <br></br>	

	3. Click _**Location Icon**_, the icons represents each reviewed stylist on registered on the system, the red circle represents their raduis of coverga. By clicking te icon, you will be able to see the name of the stylist, address and radius of coverage.

		![Stylist](/img/stylist10.png) <br></br>	

* Stylist Default Addresses

	1. Click _**Users**_.

		![Stylist](/img/stylistaddress1.png) <br></br>

	2. Click _**Stylist Default Addresses And Coverages**_.

		![Stylist](/img/stylistdefaultaddd.png) <br></br>

		You will be directed to this page.

		![Stylist](/img/stylistdefaultadd1.png) <br></br>


### How to Add or Edit Stylist Radius via Admin Web Dashboard

1. Click _**Users**_.

	![Stylist](/img/stylistaddress.png) <br></br>

2. Click _**Stylist Default Addresses and Coverages**_.

	![Stylist](/img/stylistdefaultaddd.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/stylistdefaultadd1.png) <br></br>

3. Click _**Actions**_.

	![Stylist](/img/stylistdefaultadd2.png) <br></br>

3. Click _**Edit Coverage**_.

	![Stylist](/img/stylistdefaultaddd1.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/stylistdefaultaddd2.png) <br></br>

4. Input the number of radius you want to give your stylist as his/her coverage range.

	![Stylist](/img/stylistdefaultaddd2.png) <br></br>

5. Click _**Save**_.

	![Stylist](/img/stylistdefaultaddd3.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/stylistdefaultaddd4.png) <br></br>




