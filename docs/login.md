
### Logging In

* Go to Zazzazu Admin site.

* Click _**Sign In**_.

	![log](/img/login.png) <br></br>

* Input admin credentials and click _**Log In**_.

	![log](/img/login1.png) <br></br>

	You will be directed to this page below

	![log](/img/login2.png) <br></br>

* Press _**Dashboard**_ located at the upper right corner of the screen to go to Dashboard.

	![log](/img/login3.png) <br></br>

	You will see _**Dashboard**_ as your landing page.

	![log](/img/dashboard1.png) <br></br>
