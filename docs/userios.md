### Prerequisite 

1. Install Zazzazu App.

2. Register through Facebook or Register using your email address.


### How to Register through Facebook Account

1. Download Zazzazu App using your IOS Phone. Then install it. Press the icon to Run Zazzazu.

	![Zazzazu User](/img/icon.png) <br></br>

2. Press _**LOG IN WITH FACEBOOK**_.

	![Zazzazu User](/img/registrationios1.png) <br></br>

3. You will be directed to a page where you are asked if you will authorize Zazzazu to access your information via Facebook. Press _**Accept**_, you will be directed to a page where you are asked if you want to open the page in Zazzazu, Press _**Continue**_.

	![Zazzazu User](/img/registration4.png) <br></br>


	![Zazzazu User](/img/landingpage.png) <br></br>



### How to Register Using Email Address

1. Download Zazzazu App using your IOS Phone. Then install it.

	![Zazzazu User](/img/icon.png) <br></br>

2. Press _**I'm new**_.

	![Zazzazu User](/img/registrationios1.png) <br></br>

3. Fill out the information needed for registration. Make sure to fill out everything and press the button that states that you agree to "Terms of Use and Privacy Policy"

	![Zazzazu User](/img/registrationios.png) <br></br>

	If you forgot to press the button beside the statement that you agree to Zazzazu's "Terms of Use and Privacy Policy", you will get this screen.

	![Zazzazu User](/img/registeriostoupp.png) <br></br>

4. Press _**Red Arrow Button**_.

	If you forgot to press the button beside the statement that you agree to Zazzazu's "Terms of Use and Privacy Policy", you will get this screen.

	![Zazzazu User](/img/registeriostoupp.png) <br></br>

5. There are 2 output that will be seen on your IOS App. If you are under service coverage you will see the image below.

	![Zazzazu User](/img/registration2.png) <br></br>

	If you are out of service coverage, you will see the image below.

	![Zazzazu User](/img/raincheck.png) <br></br>


	* Note: If the registration is successful you will see the landing page. <br></br>

	![Zazzazu User](/img/landingpage.png) <br></br>


### Understanding Zazzazu Menu

1. Sign In to Zazzazu App by pressing _**I'm back**_.

	![Zazzazu User](/img/registrationios2.png) <br></br>

	You will be directed to this page.

	![Zazzazu User](/img/singin.png) <br></br>

2. Press _**Red Arrow Button**_.

	![Zazzazu User](/img/signin1.png) <br></br>

	You will be directed to this page.

	![Zazzazu User](/img/landingpage.png) <br></br>

3. Press _** = **_ on the upper right hand corner of the app.

	![Zazzazu User](/img/landingpage1.png) <br></br>

	![Zazzazu User](/img/landingpage2.png) <br></br>

	Here you will see the Menu of Zazzazu App. <br></br>

4. Menu Content.

	* _**My Appointments**_ - this is a list of your Zazzazu Appointments. <br></br>

	![Zazzazu User](/img/myappointments.png) <br></br>


	* _**How it Works**_ - you will see here how Zazzazu works <br></br>

	![Zazzazu User](/img/howitworks.png) <br></br>

	Press the _**Red Arrown Button**_ to read how it works.

	![Zazzazu User](/img/howitworks1.png) <br></br>

	Press the _**Red Arrown Button**_ to read how it works.

	![Zazzazu User](/img/howitworks2.png) <br></br>

	* _**Questions and Answers**_ - this is where you can read all the facts about Zazzazu. <br></br>

	![Zazzazu User](/img/qanda.png) <br></br>

	Browse down to be able to see the other Question and Answers.

	![Zazzazu User](/img/qanda1.png) <br></br>

	* _**About Us**_ - this is where you can see facts about Zazzazu history <br></br>

	![Zazzazu User](/img/aboutus.png) <br></br>

	* _**Contact Us**_ - this is where you can email Zazzazu Admin about your booked appointment/s or if you have comments about the app or the stylist/s. <br></br>

	![Zazzazu User](/img/contactus.png) <br></br>

	![Zazzazu User](/img/contactus1.png) <br></br>

	![Zazzazu User](/img/contactus2.png) <br></br>

	* _**LOGOUT**_ - this is where you can logout from the app. <br></br>

	![Zazzazu User](/img/logout.png) <br></br>



### How to Book an Appointment

1. Log in on the app. <br></br>

2. Press _**Book Your Look**_ or Press the _**Red Arrow Button**_. <br></br>

	![Zazzazu User](/img/landingpage.png) <br></br>

3. You will be directed to _**Services**_ category. This is where you can choose what category you want to book.  <br></br>

    ![Zazzazu User](/img/services.png) <br></br>

    By pressing _**Hair**_ Category, you will get the following services.

    ![Zazzazu User](/img/services1.png) <br></br>

    By pressing _**Makeup**_ Category, you will get the following services.

    ![Zazzazu User](/img/services2.png) <br></br>

    By pressing _**Makeup & Hair**_ Category, you will get the following services. <br></br>

    ![Zazzazu User](/img/services3.png) <br></br>

4. Choose the service you want to book by pressing the _**small circle**_ beside the service. You will be directed to a Calendar, Now Choose a _**Date**_ you want the style to be delivered by pressing the date on the calendar or swiping the calendar from left to right to see the next month's dates to choose from. You can also press the linked month name on the top to see the next month's dates. <br></br>

	![Zazzazu User](/img/calendar.png) <br></br>

	**NOTE**: Calendar will only offer up to 90 days advance booking. Else you will get a pop-up message indicating that Zazzazu only caters 90 days advance appointment. <br></br>

5. Press the _**Red Arrow Button**_, you will be directed to a page where you can see all the available time slots on the system. <br></br>

	![Zazzazu User](/img/timeslot.png) <br></br>

6. Press the small circle beside the timeslot you want to book. You will be directed to this page where you will see the details of your booking appointment. Here you will see the Style you are ordering, the appointment Date and Time and How much it will be, gratuity included. <br></br>

	![Zazzazu User](/img/youreallset.png) <br></br>

7. Press _**Red Arrow Button**_ to be able to see your information details. Here you will need to fillout the following information: Email (for Facebook users only), Phone number, Appointment Address, Apartment address and additional information about your hair or directions.

	![Zazzazu User](/img/yourinformation.png) <br></br>

	Scroll down by swiping downwards to see all the details that needs to be filled out if needed. <br></br>

	![Zazzazu User](/img/yourinformation1.png) <br></br>

8. Press the _**Red Arrow Button**_, to confirm the information. You will be directed to Payment section where you will need to Enter your credited Card details and the _**Promo Code**_ for style you are ordering if there are ongoing promotions you want to avail. Press _**Confirm**_. <br></br>

	![Zazzazu User](/img/payment.png) <br></br>

	Your booking is now Successful, you will see this page.

	![Zazzazu User](/img/confirmation.png) <br></br>


### How to View Your Calendar

1. Sign In to Zazzazu App.

	![Zazzazu User](/img/registrationios2.png) <br></br>

	You will be directed to this page.

	![Zazzazu User](/img/singin.png) <br></br>

2. Enter your log in information, then press the _**Red Arrow Button**_.

	![Zazzazu User](/img/singin.png) <br></br>

	You will be directed to this page.

	![Zazzazu User](/img/landingpage.png) <br></br>

3. Press _**=**_ on the upper right hand corner of the app.

	![Zazzazu User](/img/landingpage1.png) <br></br>

	![Zazzazu User](/img/landingpage2.png) <br></br>

	Here you will see the Menu of Zazzazu App. <br></br>

4. Press _**My Appointments**_. You will be directed to this page.

	![Zazzazu User](/img/appointmentlist.png) <br></br>

	All that are with Red circle under _**Upcoming Appointments**_ are current appointments.  
	Under _**Past Appointments**_ are the completed appointments. 
	Under **Cancelled Appointments** are Cancelled Appointments. <br></br>

5. Press _**Eye Icon or the Schedule itself**_ to be able to see the appointment details.

	_**Upcoming Appointments**_

	![Zazzazu User](/img/upcomingappointment.png) <br></br>


	_**Past Appointments**_ 

	![Zazzazu User](/img/pastappointment.png) <br></br>



### How to Cancel or Modify an Appointment

1. Sign In to Zazzazu App.

	![Zazzazu User](/img/registrationios2.png) <br></br>

	You will be directed to this page.

	![Zazzazu User](/img/singin.png) <br></br>

2. Enter your log in information, then press the _**Red Arrow Button**_.

	![Zazzazu User](/img/singin.png) <br></br>

	You will be directed to this page.

	![Zazzazu User](/img/landingpage.png) <br></br>

3. Press _**=**_ on the upper right hand corner of the app.

	![Zazzazu User](/img/landingpage1.png) <br></br>

	![Zazzazu User](/img/landingpage2.png) <br></br>

	Here you will see the Menu of Zazzazu App. <br></br>

4. Press _**My Appointments**_. You will be directed to this page.

	![Zazzazu User](/img/appointmentlist.png) <br></br>

	All that are with Red circle under _**Upcoming Appointments**_ are current appointments, it can be cancelled or current appointment. If it is cancelled there will be a note on the appointment details that it is cancelled. Under _**Past Appointments**_  are past or cancelled appointments. <br></br>

5. Press _**Eye Icon or the Schedule itself**_ to be able to see the appointment details.

	_**Upcoming Appointments**_

	![Zazzazu User](/img/upcomingappointment.png) <br></br>

6. Press _**Cancel**_ to cancel the appointment.

	![Zazzazu User](/img/upcomingappointment1.png) <br></br>

	You will be directed to this page. Either the _**Pop Up Alert**_ is with refund if the appointment is cancelled for _**MORETHAN**_ 6hrs or without refund if the appointment is cancelled _**WITHIN**_ 6hrs. <br></br>

	_**Within 6 hrs**_ appointment cancellation. 

	![Zazzazu User](/img/cancel.png) <br></br>

	_**Morethan 6 hrs**_ appointment cancellation. 

	![Zazzazu User](/img/cancel1.png) <br></br>

7. Press _**Yes**_ to cancel the appointment. Press _**No**_ if you don't want to cancel.

	![Zazzazu User](/img/cancel2.png) <br></br>

	You will be directed to this page confirming that the appointment is cancelled.

	![Zazzazu User](/img/cancel3.png) <br></br>	

8. Press _**Reschedule**_ if you want to reschedule the appointment again. It will go back to _**Services**_ category.



