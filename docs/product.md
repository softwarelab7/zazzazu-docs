### How to Create New Products

* There is a pre-requisite on creating new products. This is used to ensure that if the customer wants multiple services Pageboy can deliver. 

	* **Skills** - The skill/s needed for the product/service offered.

		* **Skill Time Padding** - Time padding is used when the skill needs more time than the other skills. It will be used as an extra buffer time for the stylist. See [Time Padding](paddingv2) for more information.

			* This skill Time padding is used by the system as the secondary Time padding to be used, regardless of the Appointment padding set for all products, Product Time Padding will always be the first time padding to be used. 

			* Skill time padding is used for certain products that needs more time. You can use this to have more time buffer for your stylists. 

			* When you set this Skill padding, it will only be applicable to the skill you have applied it.

			* When creating a skill you can opt to have it empty or put time in minutes.



### How to Create Skills Offered

1. Click _**Products/Services**_

	![Products/Services](/img/looks.png) <br></br>

2. Click _**Skills**_

	![Products/Services](/img/skills.png) <br></br>

	You will be directed to this page.

	![Products/Services](/img/skills1.png) <br></br>

3. Click _**Create Skill**_

	![Products/Services](/img/skills2.png) <br></br>

	You will be directed to this page.

	![Products/Services](/img/skills3.png) <br></br>

4. Fill out the name of Skill you want to put in, like _**Hair Cut**_ or _**Make Up**_ or the skill you want to put in on the services offered.

	![Products/Services](/img/skills4.png) <br></br>

5. Click _**Stylists**_, you will see a dropdown that includes all of your stylists. Click on the stylist that should have the skill. 

	![Products/Services](/img/skills4.png) <br></br>

	Select Stylist that should have that skill.

	![Products/Services](/img/skills4.png) <br></br>


6. Add Time Padding if the Skill needs to take more time, the time padding is in minutes. You can opt to get this blank as well if you want too.

	![Products/Services](/img/skills5.png) <br></br>


7. Click _**Save**_, to save the changes.

	![Products/Services](/img/skills6.png) <br></br>

	You will be directed to this page. 

	![Products/Services](/img/skills7.png) <br></br>


### How to Edit Skills offered

1. Click _**Products/Services**_

	![Products/Services](/img/looks.png) <br></br>

2. Click _**Skills**_

	![Products/Services](/img/skills.png) <br></br>

	You will be directed to this page.

	![Products/Services](/img/skills1.png) <br></br>

3. Click _**Actions**_ beside the skill you want to edit.

	![Products/Services](/img/skills8.png) <br></br>

4. Click _**Edit**_ to edit the Skill name or the Stylist who should have the skill.

	![Products/Services](/img/skills9.png) <br></br>

	You will be directed to this page.

	![Products/Services](/img/skills10.png) <br></br>

5. Click _**Save**_ to save the changes.

	![Products/Services](/img/skills11.png) <br></br>

	Then you will be directed to this page.

	![Products/Services](/img/skills12.png) <br></br>


### How to Delete a Skill

1. Click _**Catalogue**_

	![Products/Services](/img/looks.png) <br></br>

2. Click _**Skills**_

	![Products/Services](/img/skills.png) <br></br>

	You will be directed to this page.

	![Products/Services](/img/skills1.png) <br></br>

3. Click _**Actions**_ beside the skill you want to Delete.

	![Products/Services](/img/skills8.png) <br></br>

4. Click _**Delete**_ to delete the Skill.

	![Products/Services](/img/skills13.png) <br></br>

	You will be directed to this page.

	![Products/Services](/img/skills14.png) <br></br>	

5. Click _**Delete**_ to permanently delete the skill.

	![Products/Services](/img/skills15.png) <br></br>

	You will be directed to this page for confirmation.

	![Products/Services](/img/skills16.png) <br></br>


### How to Create Product with Skills

1. Click _**Products/Services**_ then click _**Looks**_, you will be directed to the image below.

	![Products/Services](/img/looks.png) <br></br>

	![Products/Services](/img/bookable.png) <br></br> 

2. Click _**Choose type**_ beside Create a new product of type. Choose _**Bookable**_.

	![Products/Services](/img/bookable1.png) <br></br> 

3. Click _**+New Product**_.

	![Products/Services](/img/bookable2.png) <br></br> 

	You will be directed to this page.

	![Products/Services](/img/bookable3.png) <br></br> 

4. There are couple of sections that you have to fillout before saving the new product let us check all the sections.

	![Products/Services](/img/bookable3.png) <br></br> 

	- **Product Details** - This is where you put in details for your products. 

	- **Categories** - This is where you put in which category your product will be put in. 

	- **Attributes** - This is where you put in how many minutes the hair style or service duration is.

	
	- **Skills** - This is where you can add up the skill/s your product needs that will determine the number of stylist your product needs.


	<br></br>	

5. Fill out **Product Details** section.

	![Products/Services](/img/bookable3.png) <br></br> 

	- **Title**- name that referred to product.

	- **UPC**- or **Universal Product Code**, this is very useful in creating barcode symbol. Make sure to put in UPC without spaces this can be used when creating product/service promotions when you want to put this on discounted price or sale. Make sure to put in unique UPC on every service/product you will create.

	- **Description**- refers to product description. This is the right venue to describe the look.

	 	**Important Note:** Refrain from copy pasting from web straight to this section, this results to rendering of style copied from online. To avoid this, paste your copied words to a text editor tool first, for Mac users use _**TextEdit**_. From a text editor tool, copy the words and then you may now paste it on this section.

	- **Is Discountable?**- this means you're allowing your product to participate on promotions that the site has to offer. If you think this applies to your product then toggle this option. 

	- **Is Active**- this means your product can be seen on the app. 

	- **Padding**- this is the time buffer for the product itself. This time padding will take effect first before the skill time padding or the appointment time padding. See [Time Padding](paddingv2) for more information about padding.

	- **Email Template**- this is where you can choose the product email template for the product. 

	<br></br>

6. Click _**Next**_ or **Categories**, select one category from the dropdown beside category. 

	![Products/Services](/img/bookable5.png) <br></br>

7. Click _**Next**_ or **Attributes**, input the duration or the time span to accomplish the certain service/product.

	![Products/Services](/img/bookable6.png) <br></br>


8. Click **Skills** to be able to add skill/s and product price.

	![Products/Services](/img/bookable6.png) <br></br>

	To add another Skill, Click _**+**_ beside currency.

	![Products/Services](/img/bookable7.png) <br></br>

	You will be directed to this page

	![Products/Services](/img/bookable8.png) <br></br>

	Click **Skill dropdown** and choose from the dropdown the skill you want for the product.

	![Products/Services](/img/bookable12.png) <br></br>


	Fill out the following fields.

	- **Number of Stylist**- this represents the number of stylist with the same skill.
	- **Currency**- this one should be USD.
	- **Cost Price**- product gross price (including tax).
	- **Price (excl tax)** - product net price.
	- **Retail Price**- means recommended price of the product, this will serve as comparison of price only with existing similar products in the market. <br></br>

	
10. Click _**Save**_. When this is done, the new service/product will be added on the app under the category you choose where the product should be.

	![Products/Services](/img/bookable9.png) <br></br>

	![Products/Services](/img/bookable10.png) <br></br>



### How to Edit Product Information

1. Click _**Product/Services**_ then Click _**Looks**_, you will be directed to the image below.

	![Products/Services](/img/looks.png) <br></br>

	![Products/Services](/img/bookable.png) <br></br> 

2. Click _**Action**_ beside the service/product you want to edit information.

	![Products/Services](/img/bookable13.png) <br></br>

3. Click _**Edit**_

	![Products/Services](/img/bookable11.png) <br></br>

	You will be directed to this page.

	![Products/Services](/img/bookable14.png) <br></br> 

4. Here you can edit what section/s you want to edit. Just by clicking the section you want to edit.

	_**Edit Product Details**_ 

	![Products/Services](/img/bookable4.png) <br></br> 

	_**Attributes**_ (Duration of service/product)

	![Products/Services](/img/bookable16.png) <br></br>

	_**Category**_ (This is where the product should be under with)

	![Products/Services](/img/bookable15.png) <br></br>


	_**Skills and Pricing**_ , Edit Skills, by adding or deleting the skill needed for the product. Edit Price of your product by filling out the the boxes under Product and Price. 

	![Products/Services](/img/bookable17.png) <br></br>

	
5. Click _**Save**_ to save all the changes made on any sections edited. Once saved, all changes will be uploaded to your Zazzazu IOS app.

	![Products/Services](/img/bookable18.png) <br></br>

	You will be directed to this page

	![Products/Services](/img/bookable19.png) <br></br> 

NOTE: **View it on the site** is for future use. Also **Stock and Pricing** is not to be edited, it is system generated. <br></br> 


### How to Set Product inactive Product/Service

1. Click _**Products/Services**_ then press _**Looks**_, you will be directed to the image below.

	![Products/Services](/img/looks.png) <br></br>

	![Products/Services](/img/bookable.png) <br></br> 

2. Click _**Action**_ beside the service/product you want to _**Set as inactive**_. 


	![Products/Services](/img/bookable13.png) <br></br> 

3. Click _**Set as inactive**_. 

	![Products/Services](/img/bookable20.png) <br></br> 

	You will be directed to a confirmation page wherein you have to click _**Set as inactive**_ if you want to turn the product inactive. Setting the product inactive will remove the product from the product list on the Zazzazu App.

	![Products/Services](/img/bookable21.png) <br></br> 

	After clicking _**Set as inactive**_, the product/service will be removed from Zazzazu IOS app. You will see a confirmation like the picture below.

	![Products/Services](/img/bookable22.png) <br></br> 

**NOTE:** You can set the product active again by editing the product and activating it on Product details.



















