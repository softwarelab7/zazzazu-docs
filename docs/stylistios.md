### Pre-requisite set up for a Stylist account.

1. Register as Stylist via Zazzazu app.

2. Set up your User account via Zazzazu app.

3. Wait for an Admin or Staff to set your status as a stylist.

4. Set up your work schedules via Zazzazu app. 

5. Admin or Staff should set up your Radius of Coverage. 

6. Admin or Staff should set up your skills or the services stylist will cater. <br></br>



### How to Register through Facebook Account

1. Download Zazzazu App using your IOS Phone. Then install it. Press the icon to Run Zazzazu.

	![Zazzazu User](/img/icon.png) <br></br>

2. Press _**LOG IN WITH FACEBOOK**_.

	![Zazzazu User](/img/registrationios1.png) <br></br>

3. You will be directed to a page where you are asked if you will authorize Zazzazu to access your information via Facebook. Press _**Accept**_, you will be directed to a page where you are asked if you want to open the page in Zazzazu, Press _**Continue**_.

	![Zazzazu User](/img/registration4.png) <br></br>


	![Zazzazu User](/img/landingpage.png) <br></br>

**NOTE**: To be able to be a stylist, admin will need to change your user type to stylist via Admin web dashboard. 	<br></br>



### How to Register Using Email Address

1. Download Zazzazu App using your IOS Phone. Then install it.

	![Zazzazu User](/img/icon.png) <br></br>

2. Press _**I'm new**_.

	![Zazzazu User](/img/registrationios1.png) <br></br>

3. Fill out the information needed for registration. Make sure to fill out everything and press the button that states that you agree to "Terms of Use and Privacy Policy"

	![Zazzazu User](/img/registrationios.png) <br></br>

	If you forgot to press the button beside the statement that you agree to Zazzazu's "Terms of Use and Privacy Policy", you will get this screen.

	![Zazzazu User](/img/registeriostoupp.png) <br></br>

4. Press _**Red Arrow Button**_.

	If you forgot to press the button beside the statement that you agree to Zazzazu's "Terms of Use and Privacy Policy", you will get this screen.

	![Zazzazu User](/img/registeriostoupp.png) <br></br>

5. There are 2 output that will be seen on your IOS App. If you are under service coverage you will see the image below.

	![Zazzazu User](/img/registration2.png) <br></br>

	If you are out of service coverage, you will see the image below.

	![Zazzazu User](/img/raincheck.png) <br></br>


	* Note: If the registration is successful you will see the landing page. <br></br>

	![Zazzazu User](/img/landingpage.png) <br></br>


**NOTE**: To be able to be a stylist, admin will need to change your user type to stylist via Admin web dashboard. 	<br></br>


### Setting up your Stylist Account via App

1. Sign In to Zazzazu App by pressing _**I'm back**_.

	![Zazzazu User](/img/registrationios2.png) <br></br>

	You will be directed to this page.

	![Zazzazu User](/img/singin.png) <br></br>

2. Press _**Red Arrow Button**_.

	![Zazzazu User](/img/signin1.png) <br></br>

	You will be directed to this page.

	![Zazzazu User](/img/landingpage.png) <br></br>

3. Press _** = **_ on the upper right hand corner of the app.

	![Zazzazu User](/img/landingpage1.png) <br></br>

	![Zazzazu User](/img/landingpage3.png) <br></br>

	Here you will see the Menu of Zazzazu App. <br></br>

3. Press _**Stylist Settings**_.

	![Stylist](/img/stylistmenu.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/stylistaccount.png) <br></br>

4. Press _**Pencil Icon below Location**_.

	![Stylist](/img/stylistaccount1.png) <br></br>

	Enter your location or address so that you can use it as a reference for your catered range radius of service.

	![Stylist](/img/stylistlocation.png) <br></br>

	The system will search for your address. Choose the address from the choices. You will be redirected to this page.

	![Stylist](/img/stylistaccount2.png) <br></br>


5. Set your bank account by pressing _**Pencil Icon below Bank account number**_.

	![Stylist](/img/stylistaccount3.png) <br></br>

	You will be directed to this page. Press _**+**_ on the upper right corner, to add an account number.

	![Stylist](/img/stylistaccount4.png) <br></br>

6. Enter your bank account details. Then press _**Red Arrow button**_.

	![Stylist](/img/stylistaccount5.png) <br></br>

7. Press _**Bank Account number you choose to be used**_.

	![Stylist](/img/stylistaccount6.png) <br></br>

	You will see this page.

	![Stylist](/img/stylistaccount7.png) <br></br>


8. Press _**UPDATE**_. This is to update the system of your information.

	![Stylist](/img/stylistaccount8.png) <br></br>

	Now your account is all set!



### How to A Stylist Bank Account via App

1. Sign In to Zazzazu App by pressing _**I'm back**_.

	![Zazzazu User](/img/registrationios2.png) <br></br>

	You will be directed to this page.

	![Zazzazu User](/img/singin.png) <br></br>

2. Press _**Red Arrow Button**_.

	![Zazzazu User](/img/signin1.png) <br></br>

	You will be directed to this page.

	![Zazzazu User](/img/landingpage.png) <br></br>

3. Press _** = **_ on the upper right hand corner of the app.

	![Zazzazu User](/img/landingpage1.png) <br></br>

	![Zazzazu User](/img/landingpage3.png) <br></br>

	Here you will see the Menu of Zazzazu App. <br></br>

4. Press _**Stylist Settings**_.

	![Stylist](/img/stylistmenu.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/stylistaccount7.png) <br></br>

5. Press _** the Pencil icon below Bank account number**_.

	![Stylist](/img/stylistbankios.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/stylistbankios1.png) <br></br>

6. Press _**+**_ on the upper right corner of the app.

	![Stylist](/img/stylistbankios2.png) <br></br>

	You will be directed to this page.

	![Stylist](/img/stylistbankios3.png) <br></br>

7. Enter the new bank details. Press _**Red arrow button**_ when done.

	![Stylist](/img/stylistbankios4.png) <br></br>

8. Press New Account number first, then press _**Red arrow button**_ to go to Stylist account.

	![Stylist](/img/stylistbankios5.png) <br></br>

	You will be directed to this page. Press the bank account number you want to use.

	![Stylist](/img/stylistbankios6.png) <br></br>

9. Press _**UPDATE**_ to update the new bank account to your stylist account.

	![Stylist](/img/stylistaccount8.png) <br></br>


### Setting Up/Edit Your Stylist Schedule via App

1. Sign In to Zazzazu App by pressing _**I'm back**_.

	![Zazzazu User](/img/registrationios2.png) <br></br>

	You will be directed to this page.

	![Zazzazu User](/img/singin.png) <br></br>

2. Press _**Red Arrow Button**_.

	![Zazzazu User](/img/signin1.png) <br></br>

	You will be directed to this page.

	![Zazzazu User](/img/landingpage.png) <br></br>

3. Press _** = **_ on the upper right hand corner of the app.

	![Zazzazu User](/img/landingpage1.png) <br></br>

	![Zazzazu User](/img/landingpage3.png) <br></br>

	Here you will see the Menu of Zazzazu App. <br></br>

4. Press _**My Appointments**_.

	![Stylist](/img/stylistappointment1.png) <br></br>

	You will see your calendar, this calendar is where you can see all of your appointments as a customer and stylist. Normally it is blank if it is a newly set up account. <br></br>	

4. Press _**+**_ at the upper left corner of the screen, to add or edit work schedules.

	![Stylist](/img/stylistworkios.png) <br></br>	

	Add Schedule by pressing Start time below start and End time below end, including the days of Repeat. By Pressing the small circle beside the days you want the schedule to repeat.

	<br></br>	

7. Press the  _**Red Arrow Button**_ to confirm the schedule for that day or days in repeat. After pressing the Red Arrow Button you will see your work schedule posted on the App and Admin Dashboard.

	![Stylist](/img/stylistworkios2.png) <br></br>	

	After pressing done you will see your work schedule posted.


### How to Schedule Unavailability Time.

	As we can see, our work schedule is on repeat, now if you want to go out for a day or for certain hours and don't want to move your work Schedule, you can always put Unavailability Time. This way you won't need to change your Work Schedule just apply unavailability time. <br></br>

	Here is how to do it.

1. Sign In to Zazzazu App by pressing _**I'm back**_.

	![Zazzazu User](/img/registrationios2.png) <br></br>

	You will be directed to this page.

	![Zazzazu User](/img/singin.png) <br></br>

2. Press _**Red Arrow Button**_.

	![Zazzazu User](/img/signin1.png) <br></br>

	You will be directed to this page.

	![Zazzazu User](/img/landingpage.png) <br></br>

3. Press _** = **_ on the upper right hand corner of the app.

	![Zazzazu User](/img/landingpage1.png) <br></br>

	![Zazzazu User](/img/landingpage3.png) <br></br>

	Here you will see the Menu of Zazzazu App. <br></br>

4. Press _**My Appointments**_.

	![Stylist](/img/stylistappointment1.png) <br></br>

	You will see your calendar, this calendar is where you can see all of your appointments as a customer and stylist. Normally it is blank if it is a newly set up account. <br></br>	

5. Press _**Unavailable**_ located at the lower right corner of your screen.

	![Stylist](/img/stylistunavailable.png) <br></br>

6. Press _**+**_ to add Unavailable Schedule.

	![Stylist](/img/stylistunavailable1.png) <br></br>

	You will see the image below where you can set up your unavailable time.

	![Stylist](/img/stylistunavailable2.png) <br></br>	

7. Type in the Title of your Unavailable schedule, Slide if it is for whole day or not, and Press Starts and Ends to set the date and time.

	![Stylist](/img/stylistunavailable3.png) <br></br>

	or	

	![Stylist](/img/stylistunavailable4.png) <br></br>	

8. Press the _**Red Arrow Button**_ after setting all up.

	![Stylist](/img/stylistunavailable5.png) <br></br>	

	You will be directed to this page. Now everything is set, the system will not let you have an appointment during the day and time you have set on Unavailability.

	![Stylist](/img/stylistunavailable6.png) <br></br>		